@extends('{THEME_ID}.app')
@inject('onepage', 'topaz.onepage')

@section('body')
    @foreach ($sections as $section)
        @include($section->view, ['section' => $section, 'page' => $section->page])
    @endforeach
@endsection