# Onepage module for Topaz CMS

## Installation

In config/app.php add `Topaz\Onepage\TopazOnepageServiceProvider::class` in the providers : 

```php
['providers' => [
    Topaz\Onepage\TopazOnepageServiceProvider::class,
    Topaz\Core\TopazCatchallServiceProvider::class,  // Must always end Topaz modules providers
]
```

then run in a terminal : 

```bash
$ php artisan vendor:publish --provider="Topaz\Onepage\TopazOnepageServiceProvider"
$ php artisan migrate
```



