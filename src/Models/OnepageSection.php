<?php namespace Topaz\Onepage\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Topaz\Core\Models\HasSite;
use Topaz\Core\Models\Media;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\ResourceableInterface;
use Topaz\Core\Models\ResourceableModel;
use Topaz\Core\Models\User;
use Topaz\Core\Services\RouteContext;
use Topaz\Core\Services\TopazService;

class OnepageSection extends Model {

    use SEOToolsTrait, HasSite;

	protected $table = 'onepage_sections';
//    protected $route_prefix = 'page/';
	protected $fillable = ['title', 'slug', 'page_id', 'layout', 'position'];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function __toString()
    {
        return $this->title;
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function getResourceResponse(RouteContext $context)
    {
        /** @var TopazService $topaz */
        $topaz = app('topaz');

        return view(app('topaz.sites')->getCurrentThemeView($this->layout), ['section' => $this, 'page' => $this->page]);
    }

    public function getPageColumnAttribute()
    {
        return "<a href='" . route('admin.pages.edit', $this->page_id) . "'>{$this->page->title}</a>";
    }

    public function getViewAttribute()
    {
        return app('topaz.sites')->getCurrentThemeView('sections.' . $this->layout);
    }


}
