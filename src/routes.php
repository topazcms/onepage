<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => config('topaz.admin_root') . '/onepage', 'as' => 'admin.onepage.', 'middleware' => 'logged:AdminUser'], function() {

        \Topaz\Onepage\Controllers\OnepageController::routes();

});