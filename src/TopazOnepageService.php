<?php namespace Topaz\Onepage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Topaz\Core\Models\Menu;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Models\Resource;
use Topaz\Core\Services\SitesManager;
use Topaz\Core\Services\TopazService;
use Topaz\Onepage\Models\OnepageSection;

class TopazOnepageService {

    /** @var TopazService  */
    protected $topaz;

    /** @var SitesManager  */
    protected $sites;

    public function __construct()
    {
        $this->topaz = app('topaz');
        $this->sites = app('topaz.sites');
    }

    public function page(\Topaz\Core\Services\RouteContext $context)
    {
        $sections = OnepageSection::currentSite()->with('page')->orderBy('position')->get();

        $view_name = $this->sites->getCurrentThemeView('onepage');
        if (!view()->exists($view_name)) {
            abort(404, "The view '{$view_name}' doesn't exists !");
        }

        // $this->topaz->setPageTitle("");

        return view($view_name, compact('sections'));
    }

} 