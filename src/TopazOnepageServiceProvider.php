<?php

namespace Topaz\Onepage;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Topaz\Core\Commands\TopazGenerateSuperadmin;
use Topaz\Core\Middleware\TopazLogged;
use Topaz\Core\Middleware\TopazMinify;
use Topaz\Core\Models\Menu;
use Topaz\Core\Models\Page;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Topaz\Core\Services\TopazService;
use Topaz\Onepage\Models\OnepageSection;

class TopazOnepageServiceProvider extends ServiceProvider
{
    use SEOToolsTrait;

    /**
     * @var TopazService $topaz
     */
    protected $topaz;

    protected function registerRoutes()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'topaz_onepage');
    }

    protected function registerMiddlewares()
    {
        $router = $this->app['router'];
//        $router->middleware('logged', TopazLogged::class);
    }

    protected function registerCommands()
    {
//        $this->registerCommand('superadmin', TopazGenerateSuperadmin::class);
    }

    protected function registerCommand($id, $cmd)
    {
        $this->app->singleton('command.topaz.'.$id, function ($app) use ($cmd) {
            return $app[$cmd];
        });
        $this->commands('command.topaz.'.$id);
    }

    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../migrations/' => base_path('/database/migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/../public/' => public_path('topaz'),
        ], 'assets');
    }

    protected function publishConfigs()
    {
        $this->publishes([
            __DIR__ . '/../config/topaz_onepage.php' => config_path('topaz_onepage.php')
        ], 'config');

        $this->mergeConfigFrom( __DIR__ . '/../config/topaz_onepage.php', 'topaz_onepage');
    }

    private function registerBladeDirectives()
    {
//        Blade::directive('navbaritem', function($expr) {
//            $expr = str_replace('(', '', $expr);
//            $expr = str_replace(')', '', $expr);
//            $expr = str_replace('\'', '', $expr);
//
//            $params = array_map(function($val) { return trim($val); }, explode(',', $expr));
//            dd($params);
//            $li_tag = (Route::currentRouteName() == $routename) ? 'li class="active"' : 'li';
//            return "<$li_tag><a href='".route($routename, $route_params)."'><span class='glyphicon glyphicon-$icon'></span><span class='sidebar-title'>$title</span></a></li>";
//        });
    }

    protected function registerService()
    {
        $this->app->singleton('topaz.onepage', function ($app) {
            return new TopazOnepageService();
        });

         $this->topaz->registerRouteType("Onepage", 'file-text-o', 'onepage', "Site Onepage", 'file-text-o', 'topaz.onepage', 'page');

        $this->topaz->registerAdminMenuSingleItem('file-text-o', 'Onepage', 4, 'onepage*', 'admin.onepage.sections.index');

        $this->topaz->permissions()->needPermission('onepage.manage', "Gérer les sections du Onepage");

        OnepageSection::saving(function($section) {
            if (empty($section->slug)) {
                $section->slug = str_slug($section->title);
            }
            if (empty($section->position)) {
                $lastPositionned = OnepageSection::orderBy('position', 'desc')->first();
                $section->position = ($lastPositionned === null) ? 1 : $lastPositionned->position + 1;
            }
        });

        OnepageSection::creating(function($section) {
            $section->site_id = app('topaz.sites')->getCurrentSiteId();
        });

        $this->topaz->sites()->onDuplicate(function($new_site, $source_site) {
            foreach (OnepageSection::ofSite($source_site)->get() as $section) {
                $this->topaz->sites()->duplicateEntity($new_site, $section);
            }
        });

        $this->topaz->sites()->onDelete(function($site) {
            foreach (OnepageSection::ofSite($site)->get() as $section) {
                $section->delete();
            }
        });

        $this->topaz->sites()->onThemeCreation(function($cp) {
            $layouts = config('topaz_onepage.layouts');

            foreach ($layouts as $id => $name) {
                $cp(__DIR__ . '/../resources/views/section.blade.php', 'sections/' . $id . '.blade.php');
            }
            $cp(__DIR__ . '/../resources/views/onepage.blade.php', 'onepage.blade.php');
        });

    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->topaz = app('topaz');
        $this->registerRoutes();
        $this->registerViews();
        $this->registerMiddlewares();
        $this->registerCommands();
        $this->registerBladeDirectives();
        $this->registerService();

        $this->publishAssets();
        $this->publishConfigs();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
