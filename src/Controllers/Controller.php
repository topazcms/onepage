<?php namespace Topaz\Onepage\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Topaz\Core\Models\User;

abstract class Controller extends BaseController {

    use DispatchesJobs, ValidatesRequests;

    protected $request;
    protected $auth;

    /** @var User $user */
    protected $user;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->auth = app('auth')->admin();
        $this->user = $this->auth->get();
    }

}
