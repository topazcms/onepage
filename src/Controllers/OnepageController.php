<?php namespace Topaz\Onepage\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Topaz\Core\Controllers\Controller;
use Topaz\Core\Controllers\TopazCrudController;
use Topaz\Core\CrudForm\CrudForm;
use Topaz\Core\CrudForm\Select2;
use Topaz\Core\CrudForm\Text;
use Topaz\Core\Models\AdminUser;
use Topaz\Core\Models\Page;
use Topaz\Core\Models\User;
use Topaz\Onepage\Models\OnepageSection;

class OnepageController extends TopazCrudController {

    protected $modelName = OnepageSection::class;
    protected $route_prefix = 'admin.onepage.sections';

    protected $object_name_singular = 'section';
    protected $object_name_plural = 'sections';
    protected $object_name_male = false;

    protected $color = 'success';

    protected $index_table = [
        'Titre' => 'title',
        'Ordre' => 'position',
        'Page' => 'page_column',
    ];
    protected $index_sorting = [
        'Titre' => 'title',
        'Ordre' => 'position',
        'Page' => 'page_id',
    ];

    protected $validation_rules = [
        'title' => 'required',
        'page_id' => 'required|exists:topaz_pages,id',
        'position' => 'numeric',
        'layout' => 'required',
    ];

    //protected $update_validation_rules = [
    //    'position' => 'numeric|unique:onepage_sections,position,',
    //];

    protected $orderBy = ['updated_at', 'desc'];
    protected $objectsPerPage = 20;

    public static function routes()
    {
        parent::crud_routes('sections', 'sections.', ['middleware' => 'permission:onepage.manage']);
    }

    public function globalQuery($query)
    {
        return $query->currentSite();
    }

    public function indexQuery($query)
    {
        return $query->with('page');
    }

    public function searchQuery($query, $term)
    {
        $like_term = "%$term%";
        return $query->where(function ($q) use ($like_term) {
            $q->orWhere('title', 'like', $like_term);
        });
    }


    public function beforeAddForm($object)
    {
        //$object->author_id = $this->user->id;
    }

    /**
     * @param $object
     * @return CrudForm
     */
    public function getForm($object, $add)
    {
        $form = new CrudForm;

        $form->appendMain(with(new Text('title', null))->setPlaceholder('Titre de la section')->big());
        $form->appendMain(with(new Select2('page_id', Page::currentSite()->lists('title', 'id')->toArray(), "<i class=\"fa fa-file\"></i> Page")));

        $form->appendSidebar(with(new Text('slug', "Identifieur")));
        $form->appendSidebar(with(new Text('position', "Ordre")));
        $form->appendSidebar(with(new Select2('layout', config('topaz_onepage.layouts', ['default'=>'Par défaut']), "<i class=\"fa fa-list-alt mr5\"></i> Mise en page"))->noSearch());

        return $form;
    }
}
